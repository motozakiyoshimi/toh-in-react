import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getHeroesState } from '../redux/reducers'
import getHeroes from './getHeroes';
import './Heroes.scss'

class Heroes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    };
  }

  componentDidMount() {
    this.props.getHeroes();
  }

  render() {
    return (
      <div className="heroes-component">
        <h2>My Heroes</h2>
        <label>Hero name:
          <input value={this.state.name} onChange={event => this.setName(event.target.value)}></input>
        </label>
        <button onClick={() => this.addHero()}>
          add
        </button>

        {
          this.props.pending ?
            <div>Loading.....</div> :
            <ul className="heroes">
              {this.props.heroes.map(hero => <li key={hero.id}>
                <Link to={`/detail/${hero.id}`} params={{ id: hero.id }}>
                  <span className="badge">{hero.id}</span> {hero.name}
                </Link>
                <button className="delete" title="delete hero" onClick={() => this.delete(hero)}>x</button>
              </li>
              )}
            </ul>
        }
      </div>
    )
  }

  /**
   * Keep track of name input value.
   */
  setName(value) {
    this.setState({ name: value });
  }

  handleChange(event, id) {
    // create a copy of array
    const heroes = this.state.heroes.slice();
    // find the hero to change
    const itemIdx = heroes.findIndex(hero => hero.id === id);
    // create new object to replace old hero
    heroes[itemIdx] = {
      ...heroes[itemIdx],
      name: event.target.value
    }

    this.setState({ selectedHero: heroes[itemIdx], heroes });
  }

  onSelect(hero) {
    this.setState({ selectedHero: hero });
  }
}

const mapStateToProps = getHeroesState;
const mapDispatchToProps = dispatch => bindActionCreators({
  getHeroes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);
