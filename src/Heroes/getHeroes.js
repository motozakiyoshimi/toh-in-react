import axois from 'axios'
import { getHeroesPending, getHeroesSuccess, getHeroesError } from "../redux/action";


export default function getHeroes() {
  return dispatch => {
    dispatch(getHeroesPending());
    get()
      .then(data => {
        dispatch(getHeroesSuccess(data));
        return data;
      })
      .catch(error => dispatch(getHeroesError(error)));
  };
}

// mock api retrieve
const MOCK_HEROES = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
function get() {
  return axois.get('/dev/api').then(response => {
    console.log('response', response);
    return response.data.heroes;
  });
}
