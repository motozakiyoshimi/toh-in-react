export function bareActionCreator(type) {
  return () => ({ type });
}

export function standardActionCreator(type) {
  return payload => ({ type, payload });
}

export function errorActionCreator(type) {
  return error => ({ type, error });
}

export function getDataSuccessActionCreator(type) {
  return data => ({ type, data });
}