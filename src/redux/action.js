import {
  GET_HEROES_PENDING, GET_HEROES_SUCCESS, GET_HEROES_ERROR,
  GET_HERO_DETAIL_PENDING, GET_HERO_DETAIL_SUCCESS, GET_HERO_DETAIL_ERROR
} from './actionType';
import { bareActionCreator, standardActionCreator } from './util';

export const getHeroDetailPending = bareActionCreator(GET_HERO_DETAIL_PENDING);
export const getHeroDetailSuccess = standardActionCreator(GET_HERO_DETAIL_SUCCESS);
export const getHeroDetailError = standardActionCreator(GET_HERO_DETAIL_ERROR);

export const getHeroesPending = bareActionCreator(GET_HEROES_PENDING);
export const getHeroesSuccess = standardActionCreator(GET_HEROES_SUCCESS);
export const getHeroesError = standardActionCreator(GET_HEROES_ERROR);
