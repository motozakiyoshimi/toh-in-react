import { GET_HEROES_SUCCESS, GET_HEROES_PENDING, GET_HEROES_ERROR } from '../actionType';

export default function heroes(state = { pending: true, heroes: [] }, action) {
  switch (action.type) {
    case GET_HEROES_PENDING:
      return { ...state, pending: true };
    case GET_HEROES_SUCCESS:
      return { ...state, pending: false, heroes: action.payload };
    case GET_HEROES_ERROR:
      return { ...state, pending: false, error: action.payload };

    default:
      return state;
  }
}
