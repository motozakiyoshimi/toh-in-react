import { GET_HERO_DETAIL_SUCCESS, GET_HERO_DETAIL_PENDING, GET_HERO_DETAIL_ERROR } from '../actionType';

export default function hero(state = { pending: true, hero: undefined }, action) {
  switch (action.type) {
    case GET_HERO_DETAIL_PENDING:
      return { ...state, pending: true };
    case GET_HERO_DETAIL_SUCCESS:
      return { ...state, pending: false, hero: action.payload };
    case GET_HERO_DETAIL_ERROR:
      return { ...state, pending: false, error: action.payload };

    default:
      return state;
  }
}
