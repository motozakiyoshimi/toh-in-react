import { combineReducers } from 'redux'

import heroes from './heroes';
import hero from './hero';

export default combineReducers({ heroes, hero });

// the selectors
export const getHeroesState = state => state.heroes;
export const getHeroDetailState = state => state.hero;