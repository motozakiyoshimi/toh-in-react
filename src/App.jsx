import React from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom'

import './App.css';
import Heroes from './Heroes/Heroes';
import HeroDetail from './HeroDetail/HeroDetail';

function App() {
  const title = 'Tour of Heroes';

  return (
    <Router>
      <div className="App">
        <h1>{title}</h1>
        <nav>
          <Link to="/dashboard">Dashboard</Link>
          <Link to="/heroes">Heroes</Link>
        </nav>
        <Route exact path="/" component={Heroes}></Route>
        <Route path="/heroes" component={Heroes}></Route>
        <Route path="/detail/:heroId" component={HeroDetail}></Route>
      </div>
    </Router>
  );
}

export default App;
