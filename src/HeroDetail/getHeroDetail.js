import { getHeroDetailPending, getHeroDetailSuccess, getHeroDetailError } from "../redux/action";


export default function getHeroDetail(id) {
  return dispatch => {
    dispatch(getHeroDetailPending());
    get(id)
      .then(data => {
        dispatch(getHeroDetailSuccess(data));
        return data;
      })
      .catch(error => dispatch(getHeroDetailError(error)));
  };
}

// mock api retrieve
const MOCK_HEROES = [
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
function get(id) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(MOCK_HEROES.find(hero => hero.id === id))
    }, 1000);
  });
}
