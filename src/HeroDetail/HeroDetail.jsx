import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getHeroDetailState } from '../redux/reducers'
import getHeroDetail from './getHeroDetail';
import './HeroDetail.scss'

class HeroDetail extends Component {
  state = {};

  componentDidMount() {
    const heroId = +this.props.match.params.heroId;
    this.props.getHeroDetail(heroId);
  }

  render() {
    const { hero, pending } = this.props;

    return (
      <div className="hero-detail-component">
        {pending ?
          <div>Loading.....</div> : <div>
            <h2>{hero.name} Details</h2>
            <div><span>id: </span>{hero.id}</div>
            <div>
              <label>name:
              <input defaultValue={hero.name} placeholder="name" onChange={event => this.setName(event.target.value)} />
              </label>
            </div>
            <button onClick={() => this.goBack()}>go back</button>
            <button onClick={() => this.save()}>save</button>
          </div>}
      </div>
    );
  }
  goBack() {
    this.props.history.goBack();
  }
  save() {
    console.log('save hero', this.state.name);
   }

  /**
   * Keep track of name input value.
   */
  setName(value) {
    this.setState({ name: value });
  }
}

const mapStateToProps = getHeroDetailState;
const mapDispatchToProps = dispatch => bindActionCreators({
  getHeroDetail
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HeroDetail);
